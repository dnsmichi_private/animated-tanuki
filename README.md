# GitLab Tanuki Logo Animation

This project borrows the SVG and SCSS code from [this codepen](https://codepen.io/heyMP/pen/LNjeOM)
and provides instructions to convert SCSS into CSS inspired by [this tutorial](https://medium.com/@brianhan/watch-compile-your-sass-with-npm-9ba2b878415b).

## Preparations

Install nodejs with Homebrew.

```
brew install node
```

Clone this project.

```
git clone https://gitlab.com/dnsmichi/animated-tanuki.git && cd animated-tanuki
```

Install the node dependencies into this project.

```
npm install -D node-sass nodemon
```

## Build CSS

Run the `build-css` script in this project.

```
npm run build-css
```

## Watch Sass

Run

```
npm run watch-css
```

and edit `scss/logo.css` to auto-update `public/css/logo.css`.


## View

Open `public/index.html` in your local browser.

## Upload

Copy the generated `logo.css` file and embed the SVG into the main HTML page.

```
rsync public/css/logo.css root@everyonecancontribute.com:/docker/letsencrypt-docker-nginx/src/production/production-site/assets/stylesheets/logo.css
```

## Demo

A demo is embedded into https://everyonecancontribute.com 
